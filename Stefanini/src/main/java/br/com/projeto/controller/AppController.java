package br.com.projeto.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.projeto.model.Autor;
import br.com.projeto.model.Livro;
import br.com.projeto.service.AutorService;
import br.com.projeto.service.LivroService;

@Controller
public class AppController {

	@Autowired
	private AutorService autorService;

	@Autowired
	private LivroService livroService;

	@RequestMapping("/")
	public String viewHomePage(Model model) {
		List<Autor> listProducts = autorService.listAll();
		List<Livro> listLivros = livroService.listAll();

		model.addAttribute("listAuthors", listProducts);
		model.addAttribute("listLivros", listLivros);
	
		return "index";
	}
}
