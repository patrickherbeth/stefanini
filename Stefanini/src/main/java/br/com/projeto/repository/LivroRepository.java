package br.com.projeto.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.projeto.model.Livro;

@Repository
public interface LivroRepository extends JpaRepository<Livro, Long> {
	
	@Query("FROM Livro c " + "WHERE LOWER(c.name) like %:searchTerm% " + "OR LOWER(c.description) like %:searchTerm%")
	Page<Livro> search(@Param("searchTerm") String searchTerm, Pageable pageable);

}
