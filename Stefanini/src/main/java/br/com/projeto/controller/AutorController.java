package br.com.projeto.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.com.projeto.model.Autor;
import br.com.projeto.model.Livro;
import br.com.projeto.service.AutorService;

@Controller
public class AutorController {

	@Autowired
	private AutorService autorService;

	@RequestMapping("/new")
	public String showNewAutorPage(Model model) {
		Autor autor = new Autor();
		model.addAttribute("autor", autor);

		return "new_autor";
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String saveAutor(@ModelAttribute("autor") Autor autor) {
		autorService.save(autor);

		return "redirect:/";
	}

	@RequestMapping("/edit/{id}")
	public ModelAndView showEditAutorPage(@PathVariable(name = "id") int id) {
		ModelAndView mav = new ModelAndView("edit_autor");
		Autor autor = autorService.get(id);
		mav.addObject("autor", autor);

		return mav;
	}

	@RequestMapping("/delete/{id}")
	public String deleteAutor(@PathVariable(name = "id") int id) {
		autorService.delete(id);
		return "redirect:/";
	}

	@RequestMapping("/newlivro")
	public String showNewLivroPage(Model model) {
		Livro livro = new Livro();
		List<Autor> listAutor = autorService.listAll();

		model.addAttribute("listAuthors", listAutor);
		model.addAttribute("livro", livro);

		return "new_livro";
	}

	@GetMapping
	public Page<Autor> getAll() {
		return autorService.findAll();
	}
}
