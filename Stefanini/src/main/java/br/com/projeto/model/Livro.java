package br.com.projeto.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Livro {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "name")
	@NotEmpty(message = "Name required")
	private String name;

	@Column(name = "description")
	@Size(min = 1, max = 16, message = "Maximum value 240")
	private String description;

	@Column(name = "image")
	@NotEmpty(message = "Name required")
	private String image;

	@Column(name = "publicationdate")
	@NotEmpty(message = "Name required")
	private String publicationDate;

	@Column(name = "exposuredate")
	@NotEmpty(message = "Name required")
	private String exposureDate;
	
	@Column(name = "idautor")
	private int idAutor;
	
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}


	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the image
	 */
	public String getImage() {
		return image;
	}

	/**
	 * @param image the image to set
	 */
	public void setImage(String image) {
		this.image = image;
	}

	/**
	 * @return the publicationDate
	 */
	public String getPublicationDate() {
		return publicationDate;
	}

	/**
	 * @param publicationDate the publicationDate to set
	 */
	public void setPublicationDate(String publicationDate) {
		this.publicationDate = publicationDate;
	}

	/**
	 * @return the exposureDate
	 */
	public String getExposureDate() {
		return exposureDate;
	}

	/**
	 * @param exposureDate the exposureDate to set
	 */
	public void setExposureDate(String exposureDate) {
		this.exposureDate = exposureDate;
	}

	/**
	 * @return the idAutor
	 */
	public int getIdAutor() {
		return idAutor;
	}

	/**
	 * @param idAutor the idAutor to set
	 */
	public void setIdAutor(int idAutor) {
		this.idAutor = idAutor;
	}
}
