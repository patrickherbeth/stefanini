package br.com.projeto.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.projeto.model.Autor;
import br.com.projeto.model.Livro;
import br.com.projeto.repository.LivroRepository;

@Service
@Transactional
public class LivroService {

	@Autowired
	private LivroRepository repo;
	
	public List<Livro> listAll() {
		return repo.findAll();
	}
	
	public void save(Livro livro) {
		try {
			validData(livro);
			repo.save(livro);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public Livro get(long id) {
		return repo.findById(id).get();
	}
	
	public void delete(int id) {
		repo.deleteById(Long.valueOf(id));
	}
	
	public void validData(Livro livro) throws Exception {

		if (livro.getExposureDate().equals("") && livro.getPublicationDate().equals("")) {
			throw new Exception("Invalid Date empty Exposure Date or Publication Date.");
		}
	}
	
/** Paginação **/
	
	public Page<Livro> search(String searchTerm, int page, int size) {
		PageRequest pageRequest = PageRequest.of(page, size, Sort.Direction.ASC, "name");

		return repo.search(searchTerm.toLowerCase(), pageRequest);
	}

	public Page<Livro> findAll() {
		int page = 0;
		int size = 15;
		PageRequest pageRequest = PageRequest.of(page, size, Sort.Direction.ASC, "name");
		return new PageImpl<>(repo.findAll(), pageRequest, size);
	}
}
