package br.com.projeto.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.projeto.model.Autor;

@Repository
public interface AutorRepository extends JpaRepository<Autor, Long> {

	@Query("FROM Autor c " + "WHERE LOWER(c.name) like %:searchTerm% " + "OR LOWER(c.cpf) like %:searchTerm%")
	Page<Autor> search(@Param("searchTerm") String searchTerm, Pageable pageable);

}