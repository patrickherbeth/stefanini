package br.com.projeto.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.projeto.model.Autor;
import br.com.projeto.model.Livro;
import br.com.projeto.service.AutorService;
import br.com.projeto.service.LivroService;

@RestController
@RequestMapping("/page")
public class PageController {

    @Autowired
    AutorService autorService;
    
    @Autowired
    LivroService livroService;

    @GetMapping("/search_autor")
    public Page<Autor> searchAutor(
            @RequestParam("searchTerm") String searchTerm,
            @RequestParam(
                    value = "page",
                    required = false,
                    defaultValue = "0") int page,
            @RequestParam(
                    value = "size",
                    required = false,
                    defaultValue = "15") int size, Model model) {
    	    	
    	Livro livro = new Livro();
		List<Autor> listAutor = autorService.listAll();

		model.addAttribute("listAuthors", listAutor);
		model.addAttribute("livro", livro);
    	
    	System.out.println("Entrou");
		
        return autorService.search(searchTerm, page, size);

    }

    @GetMapping("/find_all_autor")
    public Page<Autor> getAll() {
        return autorService.findAll();
    }
    
    @GetMapping("/search_livro")
    public Page<Livro> searchLivro(
            @RequestParam("searchTerm") String searchTerm,
            @RequestParam(
                    value = "page",
                    required = false,
                    defaultValue = "0") int page,
            @RequestParam(
                    value = "size",
                    required = false,
                    defaultValue = "15") int size) {
        return livroService.search(searchTerm, page, size);

    }

    @GetMapping("/find_all_obras")
    public Page<Livro> getAllLivros() {
        return livroService.findAll();
    }
    
}