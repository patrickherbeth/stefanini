package br.com.projeto.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.com.projeto.model.Livro;
import br.com.projeto.service.LivroService;

@Controller
public class LivroController {

	@Autowired
	private LivroService livroService;

	@RequestMapping(value = "/savelivro", method = RequestMethod.POST)
	public String saveLivro(@ModelAttribute("livro") Livro livro) {
		livroService.save(livro);

		return "redirect:/";
	}

	@RequestMapping("/editlivro/{id}")
	public ModelAndView showEditLivroPage(@PathVariable(name = "id") int id) {
		ModelAndView mav = new ModelAndView("edit_livro");
		Livro livro = livroService.get(id);
		mav.addObject("livro", livro);

		return mav;
	}

	@RequestMapping("/deletelivro/{id}")
	public String deleteLivro(@PathVariable(name = "id") int id) {
		livroService.delete(id);
		return "redirect:/";
	}
}
