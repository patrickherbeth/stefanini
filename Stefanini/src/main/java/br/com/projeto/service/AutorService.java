package br.com.projeto.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import br.com.projeto.model.Autor;
import br.com.projeto.repository.AutorRepository;

@Service
@Transactional
public class AutorService {

	private static final int[] pesoCPF = { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };

	@Autowired
	private AutorRepository repo;

	public List<Autor> listAll() {
		return repo.findAll();
	}

	public void save(Autor autor) {
		try {
			validCpf(autor);
			validDate(autor);
			repo.save(autor);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Autor get(long id) {
		return repo.findById(id).get();
	}

	public void delete(int id) {
		repo.deleteById(Long.valueOf(id));
	}

	public void validCpf(Autor autor) throws Exception {

		if (autor.getCountry().equals("Brasil") && autor.getCpf().equals("")) {
			throw new Exception("Invalid CPF.");
		}

		isValidCPF(autor.getCpf());
	}

	private static boolean isValidCPF(String cpf) {
		cpf = cpf.trim().replace(".", "").replace("-", "");
		if ((cpf == null) || (cpf.length() != 11))
			return false;

		for (int j = 0; j < 10; j++)
			if (padLeft(Integer.toString(j), Character.forDigit(j, 10)).equals(cpf))
				return false;

		Integer digito1 = calcularDigito(cpf.substring(0, 9), pesoCPF);
		Integer digito2 = calcularDigito(cpf.substring(0, 9) + digito1, pesoCPF);
		return cpf.equals(cpf.substring(0, 9) + digito1.toString() + digito2.toString());
	}

	private static String padLeft(String text, char character) {
		return String.format("%11s", text).replace(' ', character);
	}

	private static int calcularDigito(String str, int[] peso) {
		int soma = 0;
		for (int indice = str.length() - 1, digito; indice >= 0; indice--) {
			digito = Integer.parseInt(str.substring(indice, indice + 1));
			soma += digito * peso[peso.length - str.length() + indice];
		}
		soma = 11 - soma % 11;
		return soma > 9 ? 0 : soma;
	}

	public void validDate(Autor autor) throws Exception {

		Date data = null;
		String dataTexto = autor.getBirthDate();
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		try {
			format.setLenient(false);
			data = format.parse(dataTexto);
		} catch (Exception e) {
			System.out.println("Erro Date");
		}
	}

	/** Paginação **/
	
	public Page<Autor> search(String searchTerm, int page, int size) {
		PageRequest pageRequest = PageRequest.of(page, size, Sort.Direction.ASC, "name");

		return repo.search(searchTerm.toLowerCase(), pageRequest);
	}

	public Page<Autor> findAll() {
		int page = 0;
		int size = 15;
		PageRequest pageRequest = PageRequest.of(page, size, Sort.Direction.ASC, "name");
		return new PageImpl<>(repo.findAll(), pageRequest, size);
	}
}
